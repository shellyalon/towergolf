﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float forceAdd = 1;
    public bool clicked = false;
    public Vector2 mouseClickPos;
    Vector2 lastVelocity;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clicked = true;
            Time.timeScale = 0.2f;
            mouseClickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Time.timeScale = 1f;
            if (clicked)
            {
                GiveForce();
            }
            clicked = false;
        }
    }

    public void GiveForce()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mouseClickPos - mousePos;

        float power = direction.magnitude;

        if (power > 5) rb.velocity = Vector2.zero;

        Vector2 force = direction.normalized * power * forceAdd;
        rb.AddForce(force, ForceMode2D.Impulse);
    }

    private void FixedUpdate()
    {
        lastVelocity = rb.velocity;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Wall"))
        {
            CollisionWithWall(other);
        }
    }

    void CollisionWithWall(Collision2D other)
    {
        rb.velocity = Vector3.Reflect(lastVelocity*0.75f, other.contacts[0].normal);
    }
}
